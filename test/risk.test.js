var request = require('supertest');
var app = require('../app');
var POSTData = require('../POST-data.json');

describe("risk calculation api", function () {
    it("POST a valid JSON object and expect a JSON response from the Risk Calculation API", function (done) {
        request(app).post("/api/v1.0/risk")
            .send(POSTData)
            .expect(200)
            .expect(/The probability that you get/, done)
    })
})

describe("risk calculation api", function () {
    it("POST an invalid JSON object and expect an error response from the Risk Calculation API", function (done) {
        request(app).post("/api/v1.0/risk")
            .send({})
            .expect(400)
            .expect(/Invalid/, done)
    })
})