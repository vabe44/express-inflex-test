require('dotenv').config()
var express = require('express');
var router = express.Router();
var axios = require('axios')
var cache = require('memory-cache');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var asyncHandler = require('express-async-handler')

/* POST risk listing. */
router.post('/v1.0/risk', asyncHandler(async (req, res, next) => {
    try {
        await (getTokenFromAuthorizationServer());
        if (cache.get(JSON.stringify(req.body))) { // Response already cached, serving from cache
            res.send(cache.get(JSON.stringify(req.body)));
        } else { // Response not cached yet, trying to make a request
            var calculation = await (axios.post(process.env.API_URL + '/risk-measures', req.body,
            { headers: { Authorization: 'Bearer ' + cache.get("access_token") } })) // Set auth header
            cache.put(JSON.stringify(req.body), calculation.data, 1000 * 60 * 60 * 24); // Cache the response for 24 hours
            res.send(calculation.data);
        }
    } catch (error) {
        res.status(error.response.status).json({ error: error.response.data});
    }
}));

var getTokenFromAuthorizationServer = async (function() {
    if (!cache.get("access_token")) { // No cached token found, trying to get one and cache it
        var token = await (axios.post(process.env.OAUTH_TOKEN_URL, {
            grant_type: process.env.GRANT_TYPE,
            client_id: process.env.CLIENT_ID,
            client_secret: process.env.CLIENT_SECRET
        }));
        return cache.put("access_token", token.data.access_token, token.data.expires_in * 1000);
    }
    return cache.get("access_token");
})

module.exports = router;