# Instructions

1. Installation: 'npm install'
2. Running: 'node app.js'
3. Testing: 'npm test'

# Overview

After starting the app, you can access the Risk Calculation API using the this address:
http://localhost:3000/api/v1.0/risk

Send a POST request with a valid JSON object and expect a JSON response back from the API.

# Covering Note

To complete the project, I used the following technologies:

- Node v6.5
- NPM
- Native ES5/ES6 JavaScript (No compilers)
- Express 4
- JSON
- OAuth2 Authentication (Client Credentials Grant Type)
- express-generator (generate boilerplate)
- express-async-handler (async support for express middleware)
- asyncawait (async/await support)
- axios (http requests)
- dotenv (environmental variables)
- memory-cache (cache management)
- chai (testing)
- mocha (testing)
- supertest (testing)

First I used express-generator to generate a basic express app with routing. The code for the only route for the API is in the routes/api.js file.
After receiving a POST request at the route http://localhost:3000/api/v1.0/risk, the app checks for a cached token. If it doesnt find a cached token, it tries to get one from the authorization server using the credentials in the .env file at the project root. After getting a token, it saves it to the cache until it expires, and then it will be deleted. When a valid token is obtained, the app then checks the cache again. Responses are saved to the cache for 24 hours to protect the core API from overuse using the original POST data as a key. So if the app finds a key in the cache with the identical POST data, it send back the value as a response. If it doesnt, it sends the request to the core API and saves the response to the cache first, then sends it. Any HTTP error from the auth server or the core API is caught and sent back to the user as a JSON response with the appropriate error code.
I wrote two integration tests using mocha, chai and supertest. Run them with 'npm test', they are in the file risk.test.js in the test folder.
The first tests if the authentication is working and the Risk API correctly returns a calculation using the sample data in the POST-data.json file at the project root, the second one sends invalid object to the core API and test if it throws the corresponding error.